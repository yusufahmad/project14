mport java.util.Scanner;
public class Assignment14 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner in = new Scanner (System.in);
        System.out.println("Enter integer number:");
        int num = in.nextInt();
        if (num%2 == 0){
            System.out.println(num+" is an even integer number: " );
        } else 
        {
            System.out.println(num+" is an odd integer number: "  );
        }
    }
    
}
